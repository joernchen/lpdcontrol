package main

import (
	"fmt"
	"github.com/godbus/dbus"
	"github.com/sqp/pulseaudio"
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/reader"
	driver "gitlab.com/gomidi/rtmididrv"
	"gopkg.in/yaml.v2"
	"log"
	"os"
	"strings"
	"time"
)

type Client struct {
	*pulseaudio.Client
}

func must(err error) {
	if err != nil {
		panic(err.Error())
	}
}

type Config struct {
	Sinks   []Sink
	Sources []Sink
}

var conf = Config{}

type Sink struct {
	Name       string
	Mute       uint8
	Volume     uint8
	DevicePath dbus.ObjectPath
}

func initPads() {

	data := `
sinks:
  - name: "USB Audio Line Out"
    mute: 12
    volume: 24
  - name: "USB Audio Headphones"
    mute: 13
    volume: 25
`
	err := yaml.Unmarshal([]byte(data), &conf)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	for sink := range conf.Sinks {
		dev, found := findAudioDevice(conf.Sinks[sink].Name, "Sinks")
		if found {
			conf.Sinks[sink].DevicePath = dev
		}
	}

}

var pulse, _ = pulseaudio.New()
var client = &Client{pulse}

func initPulse() {
	pulse.Register(client)
}

type printer struct{}

func (pr printer) controlChange(p *reader.Position, channel uint8, controller uint8, value uint8) {
	target := (uint32(value) + 1) * 512
	out := make([]uint32, 1)
	out[0] = target
	var device *pulseaudio.Object
	for sink := range conf.Sinks {
		if controller == conf.Sinks[sink].Volume {
			if conf.Sinks[sink].DevicePath != "" {
				device = client.Device(conf.Sinks[sink].DevicePath)
				device.Set("Volume", out)
			}
		}
	}
}
func (pr printer) noteOn(p *reader.Position, channel, key, vel uint8) {
	var device *pulseaudio.Object

	for sink := range conf.Sinks {
		if key == conf.Sinks[sink].Mute {
			if conf.Sinks[sink].DevicePath != "" {
				device = client.Device(conf.Sinks[sink].DevicePath)
				device.Set("Mute", true)
			}
		}
	}
}

func (pr printer) noteOff(p *reader.Position, channel, key, vel uint8) {
	var device *pulseaudio.Object
	for sink := range conf.Sinks {
		if key == conf.Sinks[sink].Mute {
			if conf.Sinks[sink].DevicePath != "" {
				device = client.Device(conf.Sinks[sink].DevicePath)
				device.Set("Mute", false)
			}
		}
	}
}

func listSinksAndSources() {
	listPulse("Sinks")
	listPulse("Sources")
}
func findAudioDevice(name string, devtype string) (thing dbus.ObjectPath, found bool) {
	var things []dbus.ObjectPath
	found = false
	pulse.Core().Get(devtype, &things)
	for _, thing = range things {
		dev := pulse.Device(thing)
		props, _ := dev.MapString("PropertyList") // map[string]string
		if props["device.description"] == name {
			found = true
			return
		}
	}
	return
}
func listPulse(devtype string) {
	var things []dbus.ObjectPath
	pulse.Core().Get(devtype, &things)
	log.Println("Listing " + devtype + ":")
	for _, thing := range things {
		var devname string
		//var ports []dbus.ObjectPath
		dev := pulse.Device(thing)
		dev.Get("Description", &devname)
		log.Println(devname)
		props, _ := dev.MapString("PropertyList") // map[string]string
		//testFatal(e, "get device PropertyList")
		log.Println(props["device.description"])
	}
}

func main() {
	drv, err := driver.New()
	must(err)
	initPads()
	initPulse()
	listSinksAndSources()

	// make sure to close all open ports at the end
	defer drv.Close()

	ins, err := drv.Ins()
	must(err)

	outs, err := drv.Outs()
	must(err)

	if len(os.Args) == 2 && os.Args[1] == "list" {
		printInPorts(ins)
		printOutPorts(outs)
		return
	}

	lpd, _ := findPortIn("LPD8:LPD8 MIDI", ins)

	in, out := ins[lpd], outs[lpd]

	must(in.Open())
	must(out.Open())

	//wr := writer.New(out)
	var p printer
	// listen for MIDI
	rd := reader.New(
		//reader.NoLogger(),
		reader.NoteOn(p.noteOn),
		reader.NoteOff(p.noteOff),
		reader.ControlChange(p.controlChange),
	)
	go rd.ListenTo(in)

	for {

		time.Sleep(time.Second)
	}
}

func printPort(port midi.Port) {
	fmt.Printf("[%v] %s\n", port.Number(), port.String())
}

func findPortIn(target string, ports []midi.In) (int, bool) {
	for _, port := range ports {
		fmt.Println(port.String())
		if strings.HasPrefix(port.String(), target) {
			return port.Number(), false
		}
	}
	return 0, true
}
func printInPorts(ports []midi.In) {
	fmt.Printf("MIDI IN Ports\n")
	for _, port := range ports {
		printPort(port)
	}
	fmt.Printf("\n\n")
}

func printOutPorts(ports []midi.Out) {
	fmt.Printf("MIDI OUT Ports\n")
	for _, port := range ports {
		printPort(port)
	}
	fmt.Printf("\n\n")
}
