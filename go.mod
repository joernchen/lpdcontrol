module gitlab.com/joernchen/lpdcontrol

go 1.15

require gitlab.com/gomidi/midi v1.20.2

require (
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/sqp/pulseaudio v0.0.0-20180916175200-29ac6bfa231c
	gitlab.com/gomidi/rtmididrv v0.10.1
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
